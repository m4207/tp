## Project session 3

#### Where should you be ? 

you have already implemented some basic localization and alert techniques. For some of you, 
it is GPS, for other, it is WiFi. At least you should know how to do the localization and it should
be operational with more or less precision. On the other hand, you should have done some 
implementaiton regarding alert. Basic implementation is related to battery but at some point, you should
have implement some other alarm described in the previous project session 
[here](https://gitlab.com/m4207/tp/blob/master/tp2.md).

Communication inside your group should be completely operationnal. 

#### Solutions or help for the n-2 session

If you are still stucked into the localization process, __you're doomed__. Contact the professor. Please look at the tips 
provided [here](https://gitlab.com/m4207/tp/blob/master/tp2.md) regarding localization. If you do not 
know how to use the GPS, you should give a look at the Arduino IDE example. For those who want to increase
precision by using WiFi, this is a great and good thing but you should not spend so many time on this. 
The files regarding the WiFi architecture of R&T departement are [here](https://gitlab.com/m4207/tp/tree/master/files)

__In any case, you should move on__


#### Solutions or help for the n-1 session

If you do not have any idea/code on how to raise an alert, you can use the buzzer this is 
straightforward since examples are given [here](http://wiki.seeed.cc/Grove-Buzzer/). The battery level
of the LinkIt One has only 4 stage: 100%, 66%, 33%, and 0%. That means that your alarm should be
raised depending on theses values. In order to get the battery level, some exmeple are given in the 
Arduino Studio IDE. 

You should not have any issue regarding the alert. 

#### What should you do in this session ? 

Go to the dark side of the force, enjoy free foods and cookies and conquer the world to be the master of the universe. 

You have to answer this questions: 
- __Who takes the key ?__ 

You should be able or the key should be able to know who takes the key. Basic identification is the 
student number. This information should be kept somewhere. The difficult part here is to sent the 
information from a mobile phone, a PC or whatever means you have and to make the key receive this 
information. 


#### Tips

There are a lot of ways to send an information to the key, if you have a sim card for example, drop the key an
SMS. Or the best way and since you are in the Networking and Telecommunication department, use a network. 

