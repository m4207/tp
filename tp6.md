___IMPORTANT NOTES___

L'évaluation se fera sur un entretien individuel de 5 à 10 minutes par personne durant la dernière séance. 
Chaque note sera différente pour les personnes d'un même groupe en fonction des critères données à titre 
indicatif ci-dessous. Ainsi, chaque personne doit pouvoir présenter le projet du groupe et plus particulièrement 
les aspects suivants:

- L'état du dépôt et la partie administrative du projet (Comment se sont passées les 
    communications, les issues, la répartition du travail, la structure du dépôt)
- La description du projet et des choix effectués, plus les contributions 
    personnelles de chacun (qu'est ce qui a été fait dans le projet, pour quoi ? 
    pourquoi ? Et quelle est la contribution de chacun et plus particulièrement 
    de la personne qui présente).
- Le projet. Il faut montrer la production du groupe. (Quelle est la production 
    du groupe ? Il faut mieux avoir une ou deux fonctionnalités abouties que 5 
    fonctionnalités à peine développées). 