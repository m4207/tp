## Project session 1

#### Where should you be ? 

Now you are familiar with the arduino environement and you know how to use all the
devices/sensors/actuator you have or at least understand how to use them. 
You have mastered the Arduino IDE, you know how to compile, debug, upload your code
to the board. 

Now, you also know where to find some usefull resources related to arduino and/or
LinKit One. You should have discovered where to find some documentation, useful code
explanation, projects that provide codes and maybe some debugging tips. 

#### Solutions or help for the n-2 session

If you are still in Lab 2: "__Houston, we have a problem__" please contact us ASAP. 
We are here to help you solve the problem/explain etc...

- This means that you have just installed the arduino IDE and manage to compile the blink
code. If you did not manage to test any of the board hardware, or only few parts of them due
to time restriction, you should really re-think your research procedure. 
- If you had time but did not manage to do any of the test, maybe you have mis-configured 
your IDE.
- If you spend time on specific hardware, may be the hardware is borken. You can test with 
another board. 
- If your code is not working, and do not know how to debug it, re-think your research/copy-paste
procedure. Call Mr Turquay or Myself.

#### What should you do in this session ? 

Go to the dark side of the force, enjoy free foods and cookies and conquer the world to be the master of the universe. 

You have to answer these questions/remarks: 
- __how can I localize the key ?__ 
- Once you've decided that, you should implement it. 
- You should also provide a way to access this information.

#### Tips

There are a lot of ways to localize the keys. The obvious way is to use the gps, with all its flaws (indoor precision). 
You can combine it with (accelerometer) or WiFi (ask kevin the wifi plan). 
Let us assume that you have a position. Now, you should make this position available. There differents way to do that.
__offline mechanisms__
One way is to have a WebServer running on the LinkIt One and make this information available. Another way is to
have a server somewhere, where you could send this information using any of the available communication method you have.